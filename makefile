all:
	rm -r public
	hugo --minify

install: all
	rsync -ac --delete public/ hostido:domains/jan.karwowscy.eu/public_html/
