---
title: "petrus"
repoUrl: "https://gitlab.com/jan-karwowski/petrus"
readmeUrl: "https://gitlab.com/jan-karwowski/petrus/-/blob/devel/README.md"
progLang: ["Scala"]
lastmod: "2020-03-16"
weight: 1
draft: false
---

A set of programs and libraries developed as a base of my PhD. It contains:

 - interfaces for defining deterministic and non-deterministic games
 - methods designed in my PhD -- O2UCT and Mixed-UCT that can approximate Stackelberg equilibrium in a game
 - generator of benchmark games for the method
 - several game implementations
 - auxiliary programs and scripts to process the results.
 
 
It's a typical stientific work, the code is quite messy and contains a lot of loose ends -- ideas that were initially tried but there was no time do work on it further. It's five years of my work and it's a large codebase. Small parts were written by my university coworkers.
