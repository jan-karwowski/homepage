---
title: "jack_minimixer"
repoUrl: "https://gitlab.com/jan-karwowski/jack_minimixer"
progLang: [C++]
draft: false
---
A very simple mixer for jack audio. GUI with wxWidgets.
