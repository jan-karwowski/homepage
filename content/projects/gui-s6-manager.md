---
title: "gui-s6-manager"
repoUrl: "https://gitlab.com/jan-karwowski/gui-s6-manager"
progLang: ["C++"]
draft: false
---

Gtk GUI written in C++ for checking status of, starting and stopping services supervised by [s6](https://skarnet.org/software/s6/index.html).
It is intended to control services running withing the desktop session. Definitely not intended to be used with s6 running as system-level service supervisor.
