---
title: "cpp-util"
repoUrl: "https://gitlab.com/jan-karwowski/cpp-util"
readmeUrl: "https://gitlab.com/jan-karwowski/cpp-util/-/blob/master/README.md"
progLang: ["C++"]
draft: false
weight: 1
---

Various utility classes in C++-20:
 - LRU cache
 - a simple thread pool with a task queue
 - templates to convert an int to a Boost Hana string
 
 
