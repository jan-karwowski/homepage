---
title: "parallel-image-viewer"
repoUrl: "https://gitlab.com/jan-karwowski/parallel-image-viewer"
progLang: ["C++"]
draft: false
---

An image viewer with the interface showing two images side-by-side optimized for comparing pairs of images.
Written in wxWidgets.
 
