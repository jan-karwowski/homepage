---
title: "ttt"
repoUrl: "https://bitbucket.org/jk1234567890/ttt"
progLang: ["Haskell"]
draft: false
lastmod: "2018-07-13"
---

A tic-tac-toe implementation written in Haskell with gtk gui and AI module using Monte Carlo Tree Search.
