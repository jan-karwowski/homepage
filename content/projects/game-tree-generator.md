---
title: "game-tree-generator"
repoUrl: "https://gitlab.com/jan-karwowski/game-tree-generator"
readmeUrl: "https://gitlab.com/jan-karwowski/game-tree-generator/-/blob/master/README.md?ref_type=heads"
progLang: [C++]
draft: false
weight: 1
---

A program created during the research project about testing various game playing algorithms. The goal was to create a method of generating game trees quickly and with minimal amount of memory. The project is written in C++ and uses templates to ensure high level of code inlining for better performance.
