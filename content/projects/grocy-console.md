---
title: "grocy-console"
repoUrl: "https://gitlab.com/jan-karwowski/grocy-console"
progLang: ["Scala"]
lastmod: "2024-01-12"
draft: false
---

**Work in progress.** A quick and dirty program to batch process a receipt from supermarket to add all items to [grocy](https://grocy.info/) stock using grocy API.

Also a test field for scala 3 metaprogramming and JLine library.
