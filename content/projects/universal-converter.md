---
title: "universal-converter"
repoUrl: "https://gitlab.com/jan-karwowski/universal-converter"
readmeUrl: "https://gitlab.com/jan-karwowski/universal-converter/-/blob/master/README.md"
progLang: ["C++"]
lastmod: "2024-01-14"
weight: 1
draft: false
---


`universal-converter` is a program that takes one file tree as an argument, for example your music collection, then applies a conversion command to (not necessarily) all files in the tree and writes a result to some other tree mirroring the structure of the original tree. 

Written using Boost ASIO to manage processess running in parallel.
