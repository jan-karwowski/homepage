---
title: "getnotes"
repoUrl: "https://gitlab.com/jan-karwowski/getnotes"
progLang: ["Scala", "elisp"]
lastmod: "2020-06-28"
draft: false
---

A program for extracting notes and highlight from Pocketbook reader and converting in into a format usable for communication with other people. 
