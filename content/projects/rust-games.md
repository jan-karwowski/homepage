---
title: "rust-games"
repoUrl: "https://gitlab.com/jan-karwowski/rust-games"
progLang: ["Rust"]
lastmod: "2019-12-29"
draft: false
---

A toy project to learn Rust by writing a Monte Carlo Tree Search implementation in it. The program can play tic-tac-toe, no other games are implemented.
