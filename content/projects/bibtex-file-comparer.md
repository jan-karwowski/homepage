---
title: "bibtex-file-comparer"
repoUrl: "https://gitlab.com/jan-karwowski/bibtex-file-comparer"
readmeUrl: "https://gitlab.com/jan-karwowski/bibtex-file-comparer/-/blob/master/README.md"
progLang: ["Rust"]
draft: false
---

A simple program to check if two bibtex databases contain the same entries. Used to compare exports from bibliographic databases.
