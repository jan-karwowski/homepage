---
title: "variant-json-helper"
repoUrl: "https://gitlab.com/jan-karwowski/variant-json-helper"
readmeUrl: "https://gitlab.com/jan-karwowski/variant-json-helper/-/blob/master/README.md"
progLang: ["C++"]
draft: false
weight: 1
---

A simple serializer/deserializer for `std::variant` for [Json for Modern C++ (nlohmann/json)](https://json.nlohmann.me/). 
The idea is to add `type` field to JSON object that indicates the name of the member of a variant. The approach has it's limitations described in Readme but is sufficient for my needs. 
