# Jan Karwowski

 - ✉ jan_karwowski@yahoo.com
 - [🔒︎ PGP Key]({{< resource "pubkey.asc" >}})

Software engineer interested in functional programming. I believe in DRY principle and writing concise code.
{.person-description}

 - C++
 - Java
 - Scala
 - Haskell
 - C#
 - R
{.programming-languages}


If you are interested in my career and education history please [see my resume]({{< resource "resume_en.pdf" >}}).


## Academic experience
I have 10 year working experience  at <a href="https://pw.edu.pl">Warsaw University of Technology</a> that involves teaching various CS courses and working on a PhD dissertation about computational methods for approximation of Stackelberg equilibrium in sequential games.

[See more on academia page]({{<ref "academia">}})
