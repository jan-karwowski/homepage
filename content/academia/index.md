---
title: "Academic experience"
date: 2024-01-16T15:39:08+01:00
draft: false
---

My 10 year experience as a faculty member includes the following activities.

## Research

My research mostly concencrated around computational methods in game theory. I was developing a computational method for approximating Stackelberg equilibrium in sequential games with imperfect information. With the help from my advisor I developed two methods for approximation such strategies called MixecUCT (the earlier one, applicable to zero-sum games only) and O2UCT (the newer, which works in general-sum games). This track of research resulted in writing a program that implements the mentioned methods called [petrus]({{<ref "projects/petrus">}}), a series of research papers listed below and a [PhD dissertation covering the work (in Polish)]({{<resource "monte-carlo-gry-stackelberga.pdf" >}})

### Published works

 - Karwowski J., Mańdziuk J., Żychowski A., (2023) *Sequential Stackelberg Games with bounded rationality*, Applied Soft Computing, vol. 132, article:109846
 - Karwowski J., Mańdziuk J., (2020) *Double-Oracle Sampling Method for Stackelberg Equilibrium Approximation in General-Sum Extensive-Form Games*, Proceedings of the AAAI Conference on Artificial Intelligence, DOI:10.1609/aaai.v34i02.5578 
 - Karwowski J., Mańdziuk J. (2019) *A Monte Carlo Tree Search
   approach to finding efficient patrolling schemes on
   graphs*. European Journal of Operational
   Research. Volume 277, Issue 1, Pages 255-268. DOI:10.1016/j.ejor.2019.02.017
 - Karwowski J., Mańdziuk J. (2019) *Stackelberg
   Equilibrium approximation in general-sum extensive-form games with
   double-oracle sampling method*. The 18th International Conference
   on Autonomous Agents and Multiagent Systems pp. 2045–2047 (AAMAS 2019 Extended
   abstract)
 - Karwowski J., Mańdziuk J., Żychowski A., Grajek F., An B. (2019)
   *A Memetic Approach for Sequential Security Games on a Plane with
   Moving Targets*. The Thirty Third AAAI Conference o Artificial
   Intelligence (AAAI19)
 - Karwowski J., Mańdziuk J. (2017) *The Impact of the Number of
   Averaged Attacker’s Strategies on the Results Quality in
   Mixed-UCT*. Artificial Intelligence and Soft
   Computing. ICAISC 2017. Lecture Notes in Computer Science,
   vol 10246. Springer, Cham
 - Karwowski J., Mańdziuk J. (2016) *Mixed Strategy Extraction from
   UCT Tree in Security Games*, Frontiers in Artificial Intelligence
   and Applications, Volume 285: ECAI 2016, pp. 1746-1747 (ECAI'16
   short paper)
 - Karwowski J., Mańdziuk J. (2015) *A New Approach to Security
   Games*. Artificial Intelligence and Soft
   Computing. ICAISC 2015. Lecture Notes in Computer Science,
   vol 9120. Springer, Cham
 - Karwowski J., Okulewicz M., Legierski J. (2013) *Application of
   Particle Swarm Optimization Algorithm to Neural Network Training
   Process in the Localization of the Mobile Terminal*. Engineering Applications of Neural
   Networks. EANN 2013. Communications in Computer and Information
   Science, vol 383. Springer, Berlin, Heidelberg
 - Luckner M., Karwowski J. (2017) *Estimation of Delays for Individual
   Trams to Monitor Issues in Public Transport Infrastructure*. 
   Computational Collective Intelligence. ICCCI 2017. Lecture
   Notes in Computer Science, vol 10448. Springer, Cham


## Teaching

I was teaching various CS courses about programming basics, software engineering, advanced programming and AI. Full list can be found at [the university website](https://usosweb.usos.pw.edu.pl/kontroler.php?_action=katalog2/osoby/pokazOsobe&os_id=240044), most interesting courses listed below: 

 - [Functional programming in Haskell (lecture+lab, Polish)](https://usosweb.usos.pw.edu.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&prz_kod=1120-IN000-ISP-0697)
 - [Functional programming (Haskell basics for Maths students, lab, Polish)](https://usosweb.usos.pw.edu.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&kod=1120-MAMNI-NSP-0241)
 - [Algorithms and data structures 2 (lab, advanced algorithms, graphs and others, in Polish)](https://usosweb.usos.pw.edu.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&kod=1120-IN000-ISP-0241)
 - [Computational intelligence methods in data analysis (lecture covering introduction to AI, including basic neural networks and monte carlo methods as well as overview of optimization methods)](https://usosweb.usos.pw.edu.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&kod=1120-DS000-ISP-0362)
 - [Object oriented design (an overview of OO design patterns, in Polish and English)](https://usosweb.usos.pw.edu.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&kod=1120-IN000-ISA-0243)
 - [Operating systems (POSIX api programming, in Polish and English)](https://usosweb.usos.pw.edu.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&kod=1120-IN000-ISA-0235)

## Other activities

While working at the university I was able to attend some talks and lectures even if I was not officially enrolled on the course. In particular I attendend [Category theory lecture](https://usosweb.usos.pw.edu.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&kod=1120-MAMCB-NSP-0235) and it was very elightening experience (I did it after learning Haskell) and I was seduced by the beauty of abstractions that category theory can provide for programmers.
